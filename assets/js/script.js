// Splide Promo Banners
new Splide('#splide-promo-banners', {
    type: 'loop',
    perPage: 1,
    autoplay: true,
}).mount();

// Splide Insurance Partner
new Splide('#splide-insurance-partner', {
    type: 'loop',
    perPage: 5,
    autoplay: true,
    pagination: false,
    breakpoints: {
        600: {
            perPage: 4,
            grid: {
                dimensions: [
                    [2, 2],
                    [1, 1],
                    [2, 1],
                    [1, 2],
                    [2, 2]
                ],
            }
        },
    },
}).mount();